/**
 * Created by le5h on 17.07.2016.
 */
package dataAccess;

import java.util.ArrayList;
import java.util.Scanner;

public class DataSource {

    private String _path;
    
    private File _file;
    
    private Scanner _scn;

    private ArrayList<String> _lines;
    
    DataSource(String path){

        _path = path;
        
        _file = new File(_path);

        _scn = new Scanner(_file);

        _lines = new ArrayList<String>;
        
    }

    public ArrayList<String> getAllLines(){

        _lines.add(_scn.nextLine());

    }
    
}
