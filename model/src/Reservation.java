import java.util.Date;

/**
 * Created by 1 on 17.07.2016.
 */
public class Reservation {
    private Flight flight;

    public Reservation(Flight flight) {

        this.flight = flight;
    }

    public Airport getAirportFrom() {

        return flight.getAirportFrom();
    }

    public void setAirportFrom(Airport airportFrom) {

        flight.setAirportFrom(airportFrom);
    }

    public Airport getAirportTo() {

        return flight.getAirportTo();
    }

    public void setAirportTo(Airport airportTo) {

        flight.setAirportTo(airportTo);
    }

    public Date getDateTimeFrom() {

        return flight.getDateTimeFrom();
    }

    public void setDateTimeFrom(Date dateTimeFrom) {

        flight.setDateTimeFrom(dateTimeFrom);
    }

    public Date getDateTimeTo() {

        return flight.getDateTimeTo();
    }

    public void setDateTimeTo(Date dateTimeTo) {

        flight.setDateTimeTo(dateTimeTo);
    }

    public Airline getAirline() {

        return flight.getAirline();
    }

    public void setAirline(Airline airline) {

        flight.setAirline(airline);
    }
}
