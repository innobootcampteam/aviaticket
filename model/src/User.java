import java.util.ArrayList;

/**
 * Created by 1 on 17.07.2016.
 */
public class User {
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private ArrayList<Ticket> tickets;

    public User(String firstName, String middleName, String lastName, String email) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    //// TODO: 17.07.2016 add methods for working with collection
}
