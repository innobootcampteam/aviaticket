/**
 * Created by 1 on 17.07.2016.
 */
public class Airport {
    private String name;

    public Airport(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
